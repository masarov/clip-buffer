package com.masarov.clipbuffer.util;


public interface Appender {
  void append(String text);
  String get();
}
