package com.masarov.clipbuffer.util;


public class ClipAppender implements Appender {

  protected StringBuilder buffer;

  public ClipAppender(String existingText) {
    this.buffer = new StringBuilder(existingText);
  }

  @Override
  public void append(String text) {
    buffer.append("\n").append(text);
  }

  @Override
  public String get() {
    return buffer.toString();
  }
}
