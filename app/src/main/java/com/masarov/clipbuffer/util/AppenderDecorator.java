package com.masarov.clipbuffer.util;


import java.util.Calendar;
import java.util.GregorianCalendar;

public abstract class AppenderDecorator implements Appender {

  protected Appender appender;

  public AppenderDecorator(Appender appender) {
    this.appender = appender;
  }

  @Override
  public void append(String text) {
    appender.append(text);
  }

  @Override
  public String get() {
    return appender.get();
  }

  public static class DividerDecorator extends AppenderDecorator {
    public DividerDecorator(Appender appender) {
      super(appender);
    }

    @Override
    public void append(String text) {
      super.append(text + "\n==========");
    }
  }

  public static class DateDecorator extends AppenderDecorator {
    public DateDecorator(Appender appender) {
      super(appender);
    }

    @Override
    public void append(String text) {
      super.append(text + "\n" + generateDate());
    }

    private String generateDate() {
      Calendar cal = new GregorianCalendar();
      return addZeroIfNeeded(cal.get(Calendar.DAY_OF_MONTH)) + "/" +
             addZeroIfNeeded(cal.get(Calendar.MONTH)) + "/" +
             cal.get(Calendar.YEAR) + " - " +
             addZeroIfNeeded(cal.get(Calendar.HOUR_OF_DAY)) + ":" +
             addZeroIfNeeded(cal.get(Calendar.MINUTE));
    }

    private String addZeroIfNeeded(int value) {
      return value > 9 ? String.valueOf(value) : "0" + value;
    }
  }

  public static class BreakDecorator extends AppenderDecorator {
    public BreakDecorator(Appender appender) {
      super(appender);
    }

    @Override
    public void append(String text) {
      super.append(text + "\n\n");
    }
  }

}
