package com.masarov.clipbuffer.di;


import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;

@Module
public class PreferencesModule {

  public static final String REPOSITORY = "Repository";
  public static final String GENERAL = "General";

  private static final String PREFS_NAME = "com.masarov.textbuilder.TextPrefs";

  private Application application;

  public PreferencesModule(Application application) {
    this.application = application;
  }

  @Provides
  @Singleton
  @Named(REPOSITORY)
  public SharedPreferences provideSharedPreferences() {
    return application.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
  }

  @Provides
  @Singleton
  @Named(GENERAL)
  public SharedPreferences provideGeneralSharedPreferences() {
    return PreferenceManager.getDefaultSharedPreferences(application);
  }
}
