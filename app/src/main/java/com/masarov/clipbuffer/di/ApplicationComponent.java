package com.masarov.clipbuffer.di;

import com.masarov.clipbuffer.activities.ClipActivity;
import com.masarov.clipbuffer.activities.DisplayActivity;
import dagger.Component;

import javax.inject.Singleton;

@Singleton
@Component(modules = PreferencesModule.class)
public interface ApplicationComponent {
  void inject(ClipActivity activity);

  void inject(DisplayActivity activity);
}
