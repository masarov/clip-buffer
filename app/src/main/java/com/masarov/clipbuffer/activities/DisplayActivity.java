package com.masarov.clipbuffer.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.masarov.clipbuffer.R;
import com.masarov.clipbuffer.prefs.ClipRepository;
import com.masarov.clipbuffer.MainApplication;

import javax.inject.Inject;


public class DisplayActivity extends AbstractActivity {

  @Bind(R.id.main_text_view) TextView textView;
  @Bind(R.id.fab) View fab;
  @Bind(R.id.empty_state_view) View emptyState;
  @Bind(R.id.scroll_view) View scrollView;

  @Inject ClipRepository repository;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_display);
    ButterKnife.bind(this);
  }

  @Override
  protected void onResume() {
    super.onResume();
    ((MainApplication)getApplication()).getComponent().inject(this);
    String text = repository.get();
    textView.setText(text);
    initViewVisibility(text.trim().isEmpty());
  }

  @Override
  protected void initToolbar() {
    super.initToolbar();
    if (getSupportActionBar() != null) {
      getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }
  }

  private void initViewVisibility(boolean textIsEmpty) {
    scrollView.setVisibility(textIsEmpty ? View.GONE : View.VISIBLE);
    emptyState.setVisibility(textIsEmpty ? View.VISIBLE : View.GONE);
    fab.setVisibility(textIsEmpty ? View.GONE : View.VISIBLE);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();

    if (id == R.id.action_erase) {
      AlertDialog.Builder builder = new AlertDialog.Builder(this);
      builder.setMessage(getString(R.string.clear_dialog_body))
              .setTitle(R.string.clear_dialog_header)
              .setPositiveButton(R.string.clear_dialog_clear_action, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                  repository.clear();
                  textView.setText("");
                  initViewVisibility(true);
                }
              }).setNegativeButton(R.string.clear_dialog_cancel_action, null)
              .create().show();
    }

    if (id == R.id.action_settings) {
      startActivity(new Intent(this, SettingsActivity.class));
    }

    return super.onOptionsItemSelected(item);
  }

  @OnClick(R.id.fab)
  public void shareContent() {
    Intent sendIntent = new Intent();
    sendIntent.setAction(Intent.ACTION_SEND);
    sendIntent.putExtra(Intent.EXTRA_TEXT, repository.get());
    sendIntent.setType("text/plain");
    startActivity(sendIntent);
  }
}
