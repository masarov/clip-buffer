package com.masarov.clipbuffer.activities;


import android.content.Intent;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.masarov.clipbuffer.R;

public abstract class AbstractActivity extends AppCompatActivity {

  @Bind(R.id.toolbar) Toolbar toolbar;

  @Override
  public void onContentChanged() {
    super.onContentChanged();
    initToolbar();
  }

  protected void initToolbar() {
    ButterKnife.bind(this);

    setSupportActionBar(toolbar);
    if (getSupportActionBar() != null) {
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      getSupportActionBar().setDisplayShowTitleEnabled(true);
    }
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        Intent upIntent = NavUtils.getParentActivityIntent(this);
        if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
          TaskStackBuilder.create(this)
                  .addNextIntentWithParentStack(upIntent)
                  .startActivities();
        }
        else {
          NavUtils.navigateUpTo(this, upIntent);
        }
    }
    return super.onOptionsItemSelected(item);
  }
}
