package com.masarov.clipbuffer.activities;

import android.os.Bundle;
import com.masarov.clipbuffer.R;
import com.masarov.clipbuffer.fragments.SettingsFragment;


public class SettingsActivity extends AbstractActivity {

  private static final String SETTINGS_FRAGMENT_TAG = "SettingsFragment";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_settings);
    if (getFragmentManager().findFragmentByTag(SETTINGS_FRAGMENT_TAG) == null) {
      getFragmentManager()
              .beginTransaction()
              .replace(R.id.fragment_container, new SettingsFragment(), SETTINGS_FRAGMENT_TAG)
              .commit();
    }
  }

  @Override
  protected void initToolbar() {
    super.initToolbar();
    if (getSupportActionBar() != null) {
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
  }
}
