package com.masarov.clipbuffer.activities;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;
import com.masarov.clipbuffer.util.Appender;
import com.masarov.clipbuffer.util.AppenderDecorator;
import com.masarov.clipbuffer.util.ClipAppender;
import com.masarov.clipbuffer.R;
import com.masarov.clipbuffer.prefs.ClipRepository;
import com.masarov.clipbuffer.MainApplication;
import com.masarov.clipbuffer.prefs.Settings;

import javax.inject.Inject;

public class ClipActivity extends AppCompatActivity {

  @Inject ClipRepository repository;
  @Inject Settings settings;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    ((MainApplication)getApplication()).getComponent().inject(this);
    if (Intent.ACTION_SEND.equals(getIntent().getAction()) && getIntent().hasExtra(Intent.EXTRA_TEXT)) {
      saveClip();
    }
    finish();
  }

  private void saveClip() {
    Appender appender = new ClipAppender(repository.get());
    if (settings.appendBreaks()) {
      appender = new AppenderDecorator.BreakDecorator(appender);
    }
    if (settings.appendDate()) {
      appender = new AppenderDecorator.DateDecorator(appender);
    }
    if (settings.appendDivider()) {
      appender = new AppenderDecorator.DividerDecorator(appender);
    }

    appender.append(getIntent().getStringExtra(Intent.EXTRA_TEXT));

    repository.save(appender.get());
    Toast.makeText(this, getString(R.string.chunk_saved), Toast.LENGTH_SHORT).show();
  }
}
