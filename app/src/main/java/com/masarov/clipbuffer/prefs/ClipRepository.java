package com.masarov.clipbuffer.prefs;


import android.content.SharedPreferences;
import com.masarov.clipbuffer.di.Named;

import javax.inject.Inject;

import static com.masarov.clipbuffer.di.PreferencesModule.*;

public class ClipRepository {

  public static final String TEXT_KEY = "com.masarov.textToShare";
  public static final String EMPTY_STRING = "";

  private SharedPreferences preferences;

  @Inject
  public ClipRepository(@Named(REPOSITORY) SharedPreferences preferences) {
    this.preferences = preferences;
  }

  public void save(String text) {
    preferences.edit().putString(TEXT_KEY, text).apply();
  }

  public String get() {
    return preferences.getString(TEXT_KEY, EMPTY_STRING);
  }

  public void clear() {
    preferences.edit().clear().apply();
  }

}
