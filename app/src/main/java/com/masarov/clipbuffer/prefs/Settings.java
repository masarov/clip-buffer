package com.masarov.clipbuffer.prefs;


import android.content.SharedPreferences;
import com.masarov.clipbuffer.di.Named;

import javax.inject.Inject;

import static com.masarov.clipbuffer.di.PreferencesModule.*;

public class Settings {

  private static final String DATE_KEY = "pref_date_key";
  private static final String DIVIDER_KEY = "pref_divider_key";
  private static final String BREAK_KEY = "pref_break_key";

  private SharedPreferences preferences;

  @Inject
  public Settings(@Named(GENERAL) SharedPreferences preferences) {
    this.preferences = preferences;
  }

  public boolean appendDate() {
    return preferences.getBoolean(DATE_KEY, true);
  }

  public boolean appendDivider() {
    return preferences.getBoolean(DIVIDER_KEY, true);
  }

  public boolean appendBreaks() {
    return preferences.getBoolean(BREAK_KEY, true);
  }
}
