package com.masarov.clipbuffer;

import android.app.Application;
import com.masarov.clipbuffer.di.ApplicationComponent;
import com.masarov.clipbuffer.di.DaggerApplicationComponent;
import com.masarov.clipbuffer.di.PreferencesModule;


public class MainApplication extends Application {

  private ApplicationComponent component;

  @Override
  public void onCreate() {
    super.onCreate();

    component = DaggerApplicationComponent.builder()
            .preferencesModule(new PreferencesModule(this))
            .build();
  }

  public ApplicationComponent getComponent() {
    return component;
  }
}
