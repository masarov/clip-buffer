package com.masarov.clipbuffer.ui;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Interpolator;


/**
 * Code taken from: https://github.com/ianhanniballake/cheesesquare/commit/aefa8b57e61266e4ad51bef36e669d69f7fd749c
 * Thanks to Ian Lake
 **/
public class ScrollAwareFABBehavior extends FloatingActionButton.Behavior {

  private static final Interpolator INTERPOLATOR = new FastOutSlowInInterpolator();
  private static final float ZERO = 0.0F;
  private static final float ONE = 1.0F;
  private boolean isAnimatingOut = false;

  public ScrollAwareFABBehavior(Context context, AttributeSet attrs) {
    super();
  }

  @Override
  public boolean onStartNestedScroll(final CoordinatorLayout coordinatorLayout, final FloatingActionButton child,
                                     final View directTargetChild, final View target, final int nestedScrollAxes) {
    // Ensure we react to vertical scrolling
    return nestedScrollAxes == ViewCompat.SCROLL_AXIS_VERTICAL
           || super.onStartNestedScroll(coordinatorLayout, child, directTargetChild, target, nestedScrollAxes);
  }

  @Override
  public void onNestedScroll(final CoordinatorLayout coordinatorLayout, final FloatingActionButton child,
                             final View target, final int dxConsumed, final int dyConsumed,
                             final int dxUnconsumed, final int dyUnconsumed) {

    super.onNestedScroll(coordinatorLayout, child, target, dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed);
    if (dyConsumed > 0 && !this.isAnimatingOut && child.getVisibility() == View.VISIBLE) {
      // User scrolled down and the FAB is currently visible -> hide the FAB
      animateOut(child);
    }
    else if (dyConsumed < 0 && child.getVisibility() != View.VISIBLE) {
      // User scrolled up and the FAB is currently not visible -> show the FAB
      animateIn(child);
    }
  }

  private void animateOut(final FloatingActionButton button) {
    ViewCompat.animate(button).scaleX(ZERO).scaleY(ZERO).alpha(ZERO).setInterpolator(INTERPOLATOR).withLayer()
            .setListener(new ViewPropertyAnimatorListener() {
              public void onAnimationStart(View view) {
                ScrollAwareFABBehavior.this.isAnimatingOut = true;
              }

              public void onAnimationCancel(View view) {
                ScrollAwareFABBehavior.this.isAnimatingOut = false;
              }

              public void onAnimationEnd(View view) {
                ScrollAwareFABBehavior.this.isAnimatingOut = false;
                view.setVisibility(View.GONE);
              }
            }).start();
  }

  private void animateIn(FloatingActionButton button) {
    button.setVisibility(View.VISIBLE);
    ViewCompat.animate(button).scaleX(ONE).scaleY(ONE).alpha(ONE)
            .setInterpolator(INTERPOLATOR).withLayer().setListener(null)
            .start();
  }
}
